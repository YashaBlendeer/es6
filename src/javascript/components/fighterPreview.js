import { createElement } from '../helpers/domHelper';
import { fighterDetailsMap, getFighterInfo, createFightersSelector } from './fighterSelector'; //added by me

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  // Данные отобразить с помощью функции createFighterPreview.

  // let keys = [];
  // keys = Array.from(fighterDetailsMap.keys());
  // // console.log(keys);
  let fighterInfoLeft;
  let fighterInfoRight;
  let fighterImage;
  let fighterDescription;
  // if(position === 'left' && fighter !== undefined) {
  //   fighterInfoLeft = fighterDetailsMap.get(keys[0]);
  //   console.log(fighterInfoLeft);
  //   fighterImage = createFighterImage(fighterInfoLeft);
  //   console.log(fighterImage);
  //   fighterElement.append(fighterImage, fighterInfoLeft.name);
  // } else if (position === 'right' && fighter !== undefined) {
  //   fighterInfoRight = fighterDetailsMap.get(keys[1]);
  //   console.log(fighterInfoRight);
  //   fighterImage = createFighterImage(fighterInfoRight);
  //   console.log(fighterImage);
  //   fighterElement.append(fighterImage);

  // }
  //возможно стоит заменить данные из файтерИнфо на данные из мапы
  //можно убрать файтеринфолефт и просто оставить файтер везде
  //можно переписать через ??(хз),как в селекторе
  //добавить див с описанием через криейтЭлемент
    if(position === 'left' && fighter !== undefined) {
      // fighterInfoLeft = fighter;
      fighterDescription = `Name: ${fighter.name} \n Attack: ${fighter.attack} \n Defence: ${fighter.defense} \n Health: ${fighter.health}`;
      fighterImage = createFighterImage(fighter);
      fighterElement.append(fighterImage, fighterDescription);
    } else if (position === 'right' && fighter !== undefined) {
      // fighterInfoRight = fighter;
      fighterDescription = `Name: ${fighter.name} \n Attack: ${fighter.attack} \n Defence: ${fighter.defense} \n Health: ${fighter.health}`;

      fighterImage = createFighterImage(fighter);
      fighterElement.append(fighterImage, fighterDescription);
    }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
