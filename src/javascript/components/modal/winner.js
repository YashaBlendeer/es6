import { showModal } from './modal'; //imported by me

export function showWinnerModal(fighter) {
  const title = 'the battle is ended';
  const bodyElement = `The winner is ${fighter.name}`;
  function onClose() {
    location.reload();
  }
  showModal({title, bodyElement, onClose});
}
