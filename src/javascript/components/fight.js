import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    //Функция fight должна возвращать Промис, который будет выполнен успешно, если кто-то из бойцов побеждает.
    let healthBarLeft = document.getElementById('left-fighter-indicator');
    let healthBarRight = document.getElementById('right-fighter-indicator');
    let healthLeft = firstFighter.health;
    let healthRight = secondFighter.health;
    let healthPercents = 100;
    if (healthBarRight.style.width == '' || healthBarLeft.style.width == '') {
      healthBarRight.style.width = '100%'
      healthBarLeft.style.width = '100%'
    }

    let pressedPlayerOneAttack = false;
    let pressedPlayerOneBlock = false;
    let pressedPlayerTwoAttack = false;
    let pressedPlayerTwoBlock = false;
    
    let criticalDamage;

    let pressedPlayerOneCriticalQ = false;
    let pressedPlayerOneCriticalW = false;
    let pressedPlayerOneCriticalE = false;

    let pressedPlayerTwoCriticalU = false;
    let pressedPlayerTwoCriticalI = false;
    let pressedPlayerTwoCriticalO = false;

    document.body.focus ();
    document.body.onkeyup = function () {
      pressedPlayerOneAttack = pressedPlayerOneBlock = 
      pressedPlayerTwoAttack = pressedPlayerTwoBlock = false;
    }
     
    document.body.onkeydown = function (e) {
      if (e.code == controls.PlayerOneAttack ) {
        pressedPlayerOneAttack = true;
      } else if (e.code == controls.PlayerOneBlock) {
        pressedPlayerOneBlock = true;
      } else if (e.code == controls.PlayerTwoAttack){
        pressedPlayerTwoAttack = true;
      } else if (e.code == controls.PlayerTwoBlock){
        pressedPlayerTwoBlock = true;
      } else if (e.code == controls.PlayerOneCriticalHitCombination[0]){
        pressedPlayerOneCriticalQ = true;
      } else if (e.code == controls.PlayerOneCriticalHitCombination[1]){
        pressedPlayerOneCriticalW = true;
      } else if (e.code == controls.PlayerOneCriticalHitCombination[2]){
        pressedPlayerOneCriticalE = true;
      } else if (e.code == controls.PlayerTwoCriticalHitCombination[0]){
        pressedPlayerTwoCriticalU = true;
      } else if (e.code == controls.PlayerTwoCriticalHitCombination[1]){
        pressedPlayerTwoCriticalI = true;
      } else if (e.code == controls.PlayerTwoCriticalHitCombination[2]){
        pressedPlayerTwoCriticalO = true;
      }                          
      
      if (pressedPlayerOneAttack && pressedPlayerTwoBlock) {
          pressedPlayerTwoAttack = false;
          let damage = getDamage(firstFighter, secondFighter);
          console.log('damage: ' + damage);

          if(damage != 0) {
            console.log('healthRight: ' + healthRight);
            let damagePercents = healthPercents * damage / healthRight;
            healthRight = healthRight - damage;
            console.log('healthRight: ' + healthRight);

            if(healthRight > 0) {
              healthPercents = healthPercents - damagePercents;
              healthBarRight.style.width = healthPercents + '%';
              console.log('damagePercents: ' + damagePercents);
              console.log('healthPercents: ' + healthPercents);  
            }
          }
      } else if (pressedPlayerOneAttack) {
        let damage = getDamage(firstFighter, undefined);

          if(damage != 0) {
            console.log('healthRight: ' + healthRight);
            let damagePercents = healthPercents * damage / healthRight;
            healthRight = healthRight - damage;
            console.log('healthRight: ' + healthRight);

            if(healthRight > 0) {
              healthPercents = healthPercents - damagePercents;
              healthBarRight.style.width = healthPercents + '%';
              console.log('damagePercents: ' + damagePercents);
              console.log('healthPercents: ' + healthPercents);  
            } 
          }
      } else if (pressedPlayerTwoAttack && pressedPlayerOneBlock) {
          pressedPlayerOneAttack = false;
          let damage = getDamage(secondFighter, firstFighter);
          console.log('damage: ' + damage);

          if(damage != 0) {
            console.log('healthLeft: ' + healthLeft);
            let damagePercents = healthPercents * damage / healthLeft;
            healthLeft = healthLeft - damage;
            console.log('healthLeft: ' + healthLeft);

            if(healthLeft > 0) {
              healthPercents = healthPercents - damagePercents;
              healthBarLeft.style.width = healthPercents + '%';
              console.log('damagePercents: ' + damagePercents);
              console.log('healthPercents: ' + healthPercents);  
            } 
          }
      } else if (pressedPlayerTwoAttack) {
        let damage = getDamage(secondFighter, undefined);

          if(damage != 0) {
            console.log('healthLeft: ' + healthLeft);
            let damagePercents = healthPercents * damage / healthLeft;
            healthLeft = healthLeft - damage;
            console.log('healthLeft: ' + healthLeft);

            if(healthLeft > 0) {
              healthPercents = healthPercents - damagePercents;
              healthBarLeft.style.width = healthPercents + '%';
              console.log('damagePercents: ' + damagePercents);
              console.log('healthPercents: ' + healthPercents);  
            } 
          }
      } else if (pressedPlayerOneCriticalQ && pressedPlayerOneCriticalW && pressedPlayerOneCriticalE) {
        pressedPlayerTwoBlock = false;
        criticalDamage = firstFighter.attack * 2;
        if(criticalDamage != 0) {
          console.log('healthRight: ' + healthRight);
          let damagePercents = healthPercents * criticalDamage / healthRight;
          healthRight = healthRight - criticalDamage;
          console.log('healthRight: ' + healthRight);

          if(healthRight > 0) {
            healthPercents = healthPercents - damagePercents;
            healthBarRight.style.width = healthPercents + '%';
            console.log('damagePercents: ' + damagePercents);
            console.log('healthPercents: ' + healthPercents);  
          } 
        }
      } else if (pressedPlayerTwoCriticalU && pressedPlayerTwoCriticalO && pressedPlayerTwoCriticalI) {
        pressedPlayerOneBlock = false;
        criticalDamage = secondFighter.attack * 2;
        if(criticalDamage != 0) {
          console.log('healthLeft: ' + healthLeft);
          let damagePercents = healthPercents * criticalDamage / healthLeft;
          healthLeft = healthLeft - criticalDamage;
          console.log('healthLeft: ' + healthLeft);

          if(healthLeft > 0) {
            healthPercents = healthPercents - damagePercents;
            healthBarLeft.style.width = healthPercents + '%';
            console.log('damagePercents: ' + damagePercents);
            console.log('healthPercents: ' + healthPercents);  
          } 
        }
      }

      if (healthRight <= 0) {
        console.log(`${firstFighter.name} is winner`);
        healthBarRight.style.width = 0;
          return resolve(firstFighter);
      } 
      if (healthLeft <= 0) {
        console.log(`${secondFighter} is winner`);
        healthBarLeft.style.width = 0;
          return resolve(secondFighter);
      } 
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  //Показатель здоровья бойца уменьшается на количество вреда, причиненного противником. Ее можно будет определить с помощью функции getDamage, которая будет возвращать getHitPower - getBlockPower (или 0, если боец ​​"ушел" от удара полностью, то есть сила блока больше силы удара).

  let blockPower;
  if (defender === undefined) {
    blockPower = 0;
  } else {
    blockPower = getBlockPower(defender);
  }
  let damage = getHitPower(attacker) - blockPower;

  

  if(damage < 0) {
    damage = 0;
    console.log('weak attack')
  }
  
  return damage;


}

export function getHitPower(fighter) {
  // return hit power
  //getHitPower, который бы рассчитывал силу удара (количество ущерба здоровью противника) по формуле power = attack * criticalHitChance;, где criticalHitChance - рандомно число от 1 до 2,

  const attackValue = fighter.attack;
  const criticalHitValue = getRandom(1, 2);
  const power = attackValue * criticalHitValue;
  console.log('attackValue: ' + attackValue);
  console.log('criticalHitValue: ' + criticalHitValue);
  console.log('power: ' + power);
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  //getBlockPower, который бы рассчитывал силу блока (амортизация удара противника) по формуле power = defense * dodgeChance;, где dodgeChance - рандомно число от 1 до 2. Не нужно округлять результаты данных функций.

  const blockValue = fighter.defense;
  const dodgeChance = getRandom(1, 2);
  const blockPower = blockValue * dodgeChance;

  console.log('blockValue: ' + blockValue);
  console.log('dodgeChance: ' + dodgeChance);
  console.log('blockPower: ' + blockPower);


  return blockPower;

}


//Бойцы также могут наносить критические удары, которые не могут быть заблокированы и вычисляются по формуле 2 * attack, где attack- характеристика бойца. Для того, чтобы бойцу нанести такой удар, нужно одновременно нажать 3 соответствующие клавиши, указанные в файле controls.js. Этот удар можно наносить не чаще, чем каждые 10 секунд.

//get random [1, 2]
function getRandom(min, max) {
  let criticalHitChance = Math.random() * (max - min + 1) + min;
  if (criticalHitChance > 2) {
    criticalHitChance = 2;
  }
  return criticalHitChance;
}