import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    
    //получить информацию о бойце
    
    try {
      let endpoint = await require(`../../../resources/api/details/fighter/${id}.json`);
      return endpoint;
    } catch(e) {
      alert(`Error! ${e}`)
    }

  }
}

export const fighterService = new FighterService();
